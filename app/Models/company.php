<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class company extends Model
{
    use HasFactory;
    protected $fillable = ['name','email','logo','website'];

    public static function InsertImages($images, $folder)
    {
        $image_path = $folder;
        $path = public_path() . $image_path;
        $filename = time().'_'.rand(000 ,999).'.'.$images->getClientOriginalExtension();
        $images->move($path, $filename);
        $paths = $image_path.'/'.$filename;
        return $paths;
    }

    public function employee() {
        return $this->hasMany(employee::class);
    }

}
