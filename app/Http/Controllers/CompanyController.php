<?php

namespace App\Http\Controllers;
use App\Models\company;
use DataTables;

use Illuminate\Http\Request;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(request()->ajax()){
            return DataTables::eloquent(company::query())
            ->addColumn('image', function($data)
                {
                    $url = url('/').$data->logo;
                    return $url;
                })
            ->addIndexColumn()->make(true);
        }
        return view('companies.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('companies.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'unique:companies',
            // "logo"  => "dimensions:['image','mimes:jpg,png,jpeg,gif','max_width=100,max_height=100']",
        ]);
        company::create([
            'name' => $request->name,
            'email' => $request->email,
            'logo' => !empty($request->file('logo')) ? company::InsertImages($request->file('logo'),'/company_images') : '',
            'website' => $request->website,
        ]);

        $details = [
            'title' => 'This is test mail',
            'body' => $request->name .' / '. $request->email,
        ];

        \Mail::to($request->email)->send(new \App\Mail\MyTestMail($details));
        
        return redirect('companies')->with(['add_message' => 'Record Added Successfully.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Company::where('id',$id)->first();
        return view('companies.edit',get_defined_vars());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'unique:companies,email,'.$id,
            // "logo"  => "dimensions:['image','mimes:jpg,png,jpeg,gif','max_width=100,max_height=100']",
        ]);
        $company = Company::find($id);
        $company->name = $request->name;
        $company->email = $request->email;
        $company->logo = !empty($request->file('logo')) ? company::InsertImages($request->file('logo'),'/company_images') : '';
        $company->website = $request->website;
        $company->update();
        return redirect('companies')->with(['update_message' => 'Company Updated Successfully.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $company = Company::find($id);
            $company->delete();
            return ['code'=>'200','message'=>'success'];
        }
        catch(\Exception $e){
            return ['code'=>'500','error_message'=>$e->getMessage()];
        }
    }
}
