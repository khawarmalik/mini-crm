<?php

namespace App\Http\Controllers;

use App\Models\company;
use App\Models\employee;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index(){
        $companies = company::count();
        $employees = employee::count();
        return view('layout.dashboard',get_defined_vars());
    }
}
