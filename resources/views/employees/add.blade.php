@extends('layout.master')
@section('title', 'add Employee')
@section('main')
 <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Employee Form</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('employees.index')}}">List</a></li>
              <li class="breadcrumb-item active">Add New Employee</li>
            </ol>
          </div>
        </div>
      </div>
    </section>
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Add New Employee</h3>
              </div>
              <form class="form" action="{{route('employees.store')}}" method="POST">
                @csrf
                <ul class="list-unstyled">
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
                </ul>
                <div class="card-body">
                    <div class="form-group">
                        <label for="first_name">First Name :</label>
                        <input type="text" class="form-control" name="first_name" id="first_name" placeholder="Enter First Name" value="{{ old('first_name', '') }}">
                    </div>
                    <div class="form-group">
                        <label for="last_name">Last Name :</label>
                        <input type="text" class="form-control" name="last_name" id="last_name" placeholder="Enter Last Name" value="{{ old('last_name', '') }}">
                    </div>
                    <div class="form-group">
                        <label for="company">Select Company :</label>
                        <select class="form-select form-control" id="company" name="company_id">
                            <option selected>Select Company :</option>
                            @foreach ($companies as $key => $company)
                                <option value="{{$company->id}}">{{$company->name}}</option>
                            @endforeach
                        </select>
                    </div>
                  
                  <div class="form-group">
                    <label for="email">Email address :</label>
                    <input type="email" class="form-control" name="email" id="email" placeholder="Enter email" value="{{ old('email', '') }}">
                  </div>
                  <div class="form-group">
                    <label for="phone">Phone :</label>
                    <input type="number" class="form-control" name="phone" id="phone" placeholder="enter phone Number" value="{{ old('phone', '') }}">
                  </div>
                </div>
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>
@endsection