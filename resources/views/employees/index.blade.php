@extends('layout.master')
@section('title', 'Employees List')
@section('main')
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Employees List</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{route('employees.index')}}">List</a></li>
            <li class="breadcrumb-item active">List Of Employees</li>
          </ol>
        </div>
      </div>
    </div>
  </section>
  <section class="content">
    <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              {{-- <div class="card-header">
                <h3 class="card-title">DataTable with minimal features & hover style</h3>
              </div> --}}
              <!-- /.card-header -->
              <div class="card-body">
                <table>
                    <table id="dataTable" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Sr No</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Company Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($employees as $key => $employee)    
                                <tr>
                                    <td>{{$key + 1 }}</td>
                                    <td>{{$employee->first_name}}</td>
                                    <td>{{$employee->last_name}}</td>
                                    <td>{{$employee->company->name}}</td>
                                    <td>{{$employee->email}}</td>
                                    <td>{{$employee->phone}}</td>
                                    <td>
                                        <a href="{{url("employees/")}}/{{$employee->id}}/edit" class="btn btn-sm btn-outline-primary me-1"><i class="far fa-edit nav-icon"></i></a>
                                        <a href="javascript:;" class="btn btn-sm btn-outline-danger" onclick="deleteEmployee({{$employee->id}})"><i class="fa fa-trash nav-icon"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                </table>
              </div>
            </div>
        </div>
    </div>
  </section>

@endsection

@section('script')
    @if(Session::has('add_message'))    
        <script>
            Toast.fire({
                icon: 'success',
                title: 'Employee Added Successfully.'
            });
        </script>
    @endif
    @if(Session::has('update_message'))
        <script>
            Toast.fire({
                icon: 'success',
                title: 'Employee Updated Successfully.'
            });
        </script>
    @endif

    <script>
         var dataTable;
        $(document).ready(function(){
           dataTable = $('#dataTable').DataTable();
        });

        function deleteEmployee(id) {
            var Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000
            });
            if(confirm('Are you sure you want to delete!'))
            {
                $.ajax({
                    url: "employees/" + id,
                    type: "DELETE",
                    data: {
                        _token: "{{ csrf_token() }}"
                    },
                    success: function(response) {
                        if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'Contact Admin! Somthing went wrong.'
                            });
                        }
                        else {
                            Toast.fire({
                                icon: 'success',
                                title: 'Employee Updated Successfully.'
                            });
                            setTimeout(function(){
                                window.location.reload();
                            }, 2500);
                        }
                    }
                });
            }
        }
    </script>
@endsection