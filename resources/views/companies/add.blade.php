@extends('layout.master')
@section('title', 'add company')
@section('main')
 <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Company Form</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('companies.index')}}">List</a></li>
              <li class="breadcrumb-item active">Add New Company</li>
            </ol>
          </div>
        </div>
      </div>
    </section>
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Add New Company</h3>
              </div>
              <form class="form" action="{{route('companies.store')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <ul class="list-unstyled">
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
                </ul>
                <div class="card-body">
                  <div class="form-group">
                    <label for="name">Name :</label>
                    <input type="text" class="form-control" name="name" id="name" placeholder="Enter company Name" value="{{ old('name', '') }}">
                  </div>
                  <div class="form-group">
                    <label for="email">Email address</label>
                    <input type="email" class="form-control" name="email" id="email" placeholder="Enter email" value="{{ old('email', '') }}">
                  </div>
                  <div class="form-group">
                    <label for="logo">Logo</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" name="logo" class="custom-file-input" id="logo" value="{{ old('logo', '') }}" accept="image/png, image/gif, image/jpg, image/jpeg"  >
                        <label class="custom-file-label" for="logo">Choose Logo</label>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="website">website</label>
                    <input type="text" class="form-control" name="website" id="website" placeholder="enter website" value="{{ old('website', '') }}">
                  </div>
                </div>
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>
@endsection