@extends('layout.master')
@section('title', 'Companies List')
@section('main')
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Companies List</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{route('companies.index')}}">List</a></li>
            <li class="breadcrumb-item active"></li>
          </ol>
        </div>
      </div>
    </div>
  </section>
  <section class="content">
    <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              {{-- <div class="card-header">
                <h3 class="card-title">DataTable with minimal features & hover style</h3>
              </div> --}}
              <!-- /.card-header -->
              <div class="card-body">
                <table>
                    <table id="dataTable" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Sr No</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Logo</th>
                                <th>Website</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                </table>
              </div>
            </div>
        </div>
    </div>
  </section>

@endsection

@section('script')
    @if(Session::has('add_message'))    
        <script>
            Toast.fire({
                icon: 'success',
                title: 'Company Added Successfully.'
            });
        </script>
    @endif
    @if(Session::has('update_message'))
        <script>
            Toast.fire({
                icon: 'success',
                title: 'Company Updated Successfully.'
            });
        </script>
    @endif

    <script>
         var dataTable;
        $(document).ready(function(){
            dataTable = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('companies.index') }}",
                columns: [
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        searchable: false,
                        orderable: false
                    },
                    {
                        data: 'name'
                    },
                    {
                        data: 'email',
                    },
                    { data: "image", "render": function (data){
                        return '<img src="'+data+'" style="height:80px;width:100px;"/>'
                    }},
                    // {
                    //     data: 'logo',
                    // },
                    {
                        data: 'website',
                    },
                    {
                        data: '',
                    },
                ],
                "columnDefs": [
                    {
                        // Actions
                        targets: -1,
                        searchable:false,
                        orderable: false,
                        render: function(data, type, full, meta) {
                            return (
                                '<a href="{{url("companies/")}}'+"/"+full.id+'/edit" class="btn btn-sm btn-outline-primary me-1"><i class="far fa-edit nav-icon"></i></a>'+
                                '<a href="javascript:;" class="btn btn-sm btn-outline-danger" onclick="deleteCompany('+full.id+
                                ')"><i class="fa fa-trash nav-icon"></i></a>'
                            );
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
            });
        });

        function deleteCompany(id) {
            var Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000
            });
            if(confirm('Are you sure you want to delete!'))
            {
                $.ajax({
                    url: "companies/" + id,
                    type: "DELETE",
                    data: {
                        _token: "{{ csrf_token() }}"
                    },
                    success: function(response) {
                        if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'Contact Admin! Somthing went wrong.'
                            });
                        }
                        else {
                            Toast.fire({
                                icon: 'success',
                                title: 'Company Updated Successfully.'
                            });
                            dataTable.ajax.reload();
                        }
                    }
                });
            }
        }
    </script>
@endsection